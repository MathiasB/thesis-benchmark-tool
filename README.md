Benchmarking Tool
-----------------

Benchmarking tool written in Go, to replay load on Wordpress installations.


### Installation

Clone the repository and then build it:

    $ go build


### Usage

Run `n` requests, over `c` concurrent connections:

    $ ./benchmark -n 100 -c 10 -ssr false -assets "http://51.15.119.84" -index "http://51.15.119.84"
    
Where `ssr` denotes whether the replayed load should mimic server-side rendering or client-side rendering.  
`assets` and `index` are the hostnames of the servers on which they are hosted.


### Author

[Mathias Beke](https://denbeke.be)