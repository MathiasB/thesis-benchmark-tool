package main

import "fmt"
import "time"
import "net/http"
import "flag"
import "sync"

// Worker that does the HTTP request
func worker(id int, jobs <-chan int, results chan<- int, waterfall func()) {
	for j := range jobs {

		waterfall()

		results <- j
	}
}

func Get(url string, wg *sync.WaitGroup) {
	_, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	if wg != nil {
		wg.Done()
	}
}

var assets string
var index string
var onlyRendering bool

func csr() {

	var wg sync.WaitGroup

	// Style
	//wg.Add(1)

	// Index
	Get(assets+"/wp-content/themes/foxhound/build/index.html", nil)

	// Index
	wg.Add(1)
	go Get(index+"/wp-json/wp/v2/posts/1", &wg)

	if !onlyRendering {

		// Style sheet
		wg.Add(1)
		go Get(assets+"/wp-content/themes/twentyseventeen/style.css?ver=4.9.6", &wg)
	}

	wg.Wait()

	if !onlyRendering {
		Get(assets+"/wp-content/themes/twentyseventeen/assets/images/header.jpg", nil)
	}

}

func ssr() {

	// Index
	Get(index+"/2018/05/30/hello-world/", nil)

	if !onlyRendering {

		var wg sync.WaitGroup
		wg.Add(1)
		go Get(assets+"/wp-content/themes/twentyseventeen/style.css?ver=4.9.6", &wg)

		// Some image
		wg.Add(1)
		go Get(assets+"/wp-content/themes/twentyseventeen/assets/images/header.jpg", &wg)

		wg.Wait()

	}
	//fmt.Println("Done")

}

func main() {

	n := flag.Int("n", 1000, "Number of requests")
	c := flag.Int("c", 10, "Concurrent connections")
	isSSR := flag.Bool("ssr", true, "Server-side rendering or not?")
	//url := flag.String("url", "http://localhost:1323/page/1", "URL to benchmark")
	flag.StringVar(&assets, "assets", "http://localhost:1323/", "Domain of assets")
	flag.StringVar(&index, "index", "http://localhost:1323/", "Url of index")
	flag.BoolVar(&onlyRendering, "only-rendering", false, "Only take into account rendering requests?")

	flag.Parse()

	// Chancels for tasks
	jobs := make(chan int, *n)
	results := make(chan int, *n)

	// This starts up 3 workers, initially blocked
	// because there are no jobs yet.
	for w := 1; w <= *c; w++ {
		if *isSSR {
			go worker(w, jobs, results, ssr)
		} else {
			go worker(w, jobs, results, csr)
		}

	}

	start := time.Now()

	// Send tasks to pool
	for j := 1; j <= *n; j++ {
		jobs <- j
	}
	close(jobs)

	// Loop over results
	for a := 1; a <= *n; a++ {
		<-results
	}

	elapsed := time.Since(start)

	throughput := float64(*n) / float64(elapsed) * 1000000000.0

	fmt.Printf("Throughput %f", throughput)
}
